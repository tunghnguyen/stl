#include "sort.h"

#include <utility>

using std::swap;

void bubbleSort(int arr[], int n) {}

void insertionSort(int arr[], int n) {
  int j, key;
  for (int i = 1; i < n; ++i) {
    key = arr[i];
    j = i - 1;
    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      --j;
    }
    arr[j + 1] = key;
  }
}

void selectionSort(int arr[], int n) {
  int min, temp;
  for (int i = 0; i < n - 1; ++i) {
    min = i;
    for (int j = i + 1; j < n; ++j) {
      if (arr[j] < arr[min])
        min = j;
    }
    if (min != i) {
      temp = arr[i];
      arr[i] = arr[min];
      arr[min] = temp;
    }
  }
}

static void siftDown(int arr[], int p, int r) {
  int left = 2 * p + 1;
  int right = 2 * p + 2;

  while (left <= r && right <= r) {
    // Parent is greater than both children
    if (arr[p] >= arr[left] && arr[p] >= arr[right])
      break;

    // Parent is smaller than left child
    if (arr[left] >= arr[right] && arr[p] < arr[left]) {
      swap(arr[p], arr[left]);
      p = left;
    }

    // Parent is smaller than right child
    else if (arr[right] > arr[left] && arr[p] < arr[right]) {
      swap(arr[p], arr[right]);
      p = right;
    }

    left = 2 * p + 1;
    right = 2 * p + 2;
  }

  // Parent is smaller than only child
  if (left <= r && arr[p] < arr[left])
    swap(arr[p], arr[left]);
}

void heapSort(int arr[], int n) {
  for (int i = n / 2 - 1; i >= 0; --i) {
    siftDown(arr, i, n - 1);
  }

  for (int j = n - 1; j > 0; --j) {
    swap(arr[0], arr[j]);
    siftDown(arr, 0, j - 1);
  }
}

static void merge(int arr[], int p, int q, int r) {
  int nl = q - p + 1;
  int nr = r - q;
  int arrl[nl], arrr[nr];

  // Copy the original array into two temporary arrays
  for (int i = 0; i < nl; ++i)
    arrl[i] = arr[p + i];
  for (int j = 0; j < nr; ++j)
    arrr[j] = arr[q + 1 + j];

  // Merge the two temporary arrays
  int i = 0, j = 0;
  int k = p;

  while (i < nl && j < nr) {
    if (arrl[i] <= arrr[j]) {
      arr[k] = arrl[i];
      ++i;
    } else {
      arr[k] = arrr[j];
      ++j;
    }
    ++k;
  }

  // Merge the remaining items
  while (i < nl) {
    arr[k] = arrl[i];
    ++i;
    ++k;
  }
  while (j < nr) {
    arr[k] = arrr[j];
    ++j;
    ++k;
  }
}

void mergeSort(int arr[], int p, int r) {
  if (p >= r)
    return;
  int q = (p + r) / 2;

  mergeSort(arr, p, q);
  mergeSort(arr, q + 1, r);
  merge(arr, p, q, r);
}

static int partition(int arr[], int p, int r) {
  // Choose middle pivot
  int pivot = arr[(p + r) / 2];

  // Hoare's scheme
  int i = p - 1;
  int j = r + 1;

  while (true) {
    do {
      ++i;
    } while (arr[i] < pivot);

    do {
      --j;
    } while (arr[j] > pivot);

    if (i >= j)
      return j;

    swap(arr[i], arr[j]);
  }
}

void quickSort(int arr[], int p, int r) {
  if (p >= r)
    return;

  int q = partition(arr, p, r);
  quickSort(arr, p, q);
  quickSort(arr, q + 1, r);
}
