cmake_minimum_required(VERSION 3.27.0)
project(STL VERSION 0.1.0 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_subdirectory(src)

add_library(stl
    src/sort.cpp 
    src/tree.cpp
    src/buffer.cpp
    src/list.cpp
)
target_include_directories(stl PRIVATE include)

add_executable(sort 
    src/sort.cpp
    src/sort.test.cpp
)

add_executable(tree
    src/tree.cpp
    src/tree.test.cpp
)

add_executable(buffer
    src/buffer.cpp
    src/buffer.test.cpp
)

add_executable(list
    src/list.cpp
    src/list.test.cpp
)

target_include_directories(sort PRIVATE include)
target_include_directories(tree PRIVATE include)
target_include_directories(buffer PRIVATE include)
target_include_directories(list PRIVATE include)
